package model;

import java.util.ArrayList;

import vos.RegistryValue;
import vos.Treatment;

public class StorageModel {
	public ArrayList<Treatment> treatments = new ArrayList<>();
	public ArrayList<RegistryValue> registers = new ArrayList<>();

	public void registerTreatment(Treatment treatment) {
		if (treatment == null)
			return;
		treatments.add(treatment);
	}

	public boolean containsValue(RegistryValue value) {
		return registers.stream().anyMatch(el -> el.getDocumentoPaciente().equals(value.getDocumentoPaciente()));
	}

	public void removeAllTreatments() {
		treatments.clear();
	}

	public void clearRegistry() {
		registers.clear();
	}

	public void addRegistryValue(RegistryValue value) {
		if (value == null)
			return;
		registers.add(value);
	}

	public Treatment[] getTreatments() {
		return treatments.toArray(new Treatment[0]);
	}

	public RegistryValue[] getRegisters(String queryString) {
		if (queryString == null || queryString.isBlank())
			return registers.toArray(new RegistryValue[0]);
		else
			return registers.stream().filter((el) -> el.getDocumentoPaciente().equals(queryString)).toList()
					.toArray(new RegistryValue[0]);
	}

	public double getCostoHospitalizacionDia() {
		return 80_000;
	}
}
