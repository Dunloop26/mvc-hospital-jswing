package model;

import enums.TipoOperario;
import vos.CostCalculation;

public class CostCalculationModel {

	private StorageModel model;

	public CostCalculationModel(StorageModel model) {
		this.model = model;
	}

	public double calculate(CostCalculation value) {
		double costPerDay = model.getCostoHospitalizacionDia();
		TipoOperario operatorType = TipoOperario.fromValue(value.getTipoOperario());

		double costPercent = 1;
		if (operatorType == TipoOperario.MINERO)
			costPercent = 0.8;

		return (value.getDays() * costPercent * costPerDay) + value.getMedicineCost() * costPercent
				+ value.getTreatmentCost() * costPercent;
	}
}
