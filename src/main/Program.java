package main;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.formdev.flatlaf.FlatLightLaf;

import controllers.MainViewController;
import controllers.QueryViewController;
import controllers.RegisterViewController;
import events.EventListener;
import events.EventManager;
import events.IEventManager;
import model.CostCalculationModel;
import model.StorageModel;
import view.MainView;
import view.QueryView;
import view.RegisterView;
import vos.CostCalculation;
import vos.RegistryValue;
import vos.Treatment;

public class Program {
	static Program program = new Program();

	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {

			private MainView mainView;
			private RegisterView registerView;
			private QueryView queryView;
			
			private StorageModel storageModel;
			private CostCalculationModel costCalculationModel;
			
			@Override
			public void run() {
				// Ejecuto la ventana principal del programa
				try {
					utils.UIManagerDefaults
							.setUIFont(new javax.swing.plaf.FontUIResource("DejaVu Sans", Font.BOLD, 12));
					FlatLightLaf.setup();
					UIManager.setLookAndFeel(new FlatLightLaf());
					// Ejecuto la configuración de la aplicación
					setup();
					// Muestro la ventana principal
					mainView.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			private void setup() {
				IEventManager eventManager = new EventManager();

				eventManager.addEvent("openRegisterView", new EventListener() {

					@Override
					public <T> void onEventFired(T arg) {
						registerView.setVisible(true);
					}

				});

				eventManager.addEvent("openQueryView", new EventListener() {

					@Override
					public <T> void onEventFired(T arg) {
						queryView.setVisible(true);
					}
				});

				eventManager.addEvent("registerUser", new EventListener() {

					@Override
					public <T> void onEventFired(T arg) {

						RegistryValue registryValue = (RegistryValue) arg;
						String mensaje = "";
						if (storageModel.containsValue(registryValue)) {
							mensaje = String.format("No se puede registrar el documento: %s \nEl documento ya existe.",
									registryValue.getDocumentoPaciente());
						} else {
							storageModel.addRegistryValue(registryValue);
							mensaje = String.format("Se ha registrado el paciente con el documento: %s",
									registryValue.getDocumentoPaciente());
						}

						JOptionPane.showMessageDialog(registerView, mensaje);
					}
				});

				eventManager.addEvent("requestTreatment", new EventListener() {

					@Override
					public <T> void onEventFired(T arg) {

						eventManager.fireEvent("onTreatment", storageModel.getTreatments());
					}
				});
				
				eventManager.addEvent("requestQuery", new EventListener() {

					@Override
					public <T> void onEventFired(T arg) {
						String queryString = (String)arg;
						eventManager.fireEvent("onQueryResults", storageModel.getRegisters(queryString));
					}
					
				});
				
				eventManager.addEvent("requestCostCalculation", new EventListener() {
					
					@Override
					public <T> void onEventFired(T arg) {
						CostCalculation value = (CostCalculation)arg;
						double result = costCalculationModel.calculate(value);
						eventManager.fireEvent("onCostResult", result);
					}
				});
				
				MainViewController controller = new MainViewController(eventManager);
				RegisterViewController registerViewController = new RegisterViewController(eventManager);
				QueryViewController queryViewController = new QueryViewController(eventManager);
				
				storageModel = new StorageModel();
				costCalculationModel = new CostCalculationModel(storageModel);

				storageModel.registerTreatment(new Treatment("neumo", "Neumoconiosis", 500_000));
				storageModel.registerTreatment(new Treatment("vertigo", "Vértigo", 300_000));
				storageModel.registerTreatment(new Treatment("prob_resp", "Problemas Respiratorios", 250_000));

				mainView = new MainView(controller);
				registerView = new RegisterView(mainView, registerViewController);
				queryView = new QueryView(mainView, queryViewController);
			}
		});
	}
}
