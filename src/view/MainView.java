package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controllers.MainViewController;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.Box;
import java.awt.Component;
import javax.swing.BoxLayout;
import javax.swing.SpringLayout;

public class MainView extends JFrame {

	private JPanel contentPane;
	private MainViewController controller;

	/**
	 * Create the frame.
	 */
	public MainView(MainViewController controller) {
		this.controller = controller;
		
		createComponents();
	}
	
	private void createComponents() {
		setMinimumSize(new Dimension(550, 350));
		setBounds(new Rectangle(0, 0, 0, 0));
		setBackground(Color.WHITE);
		setTitle("Clinica San Antonio S.A");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 350);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		var emptyBorder = new EmptyBorder(10,10,10,10);
		
		JPanel panelHeader = new JPanel();
		panelHeader.setBackground(Color.WHITE);
		contentPane.add(panelHeader, BorderLayout.WEST);
		GridBagLayout gbl_panelHeader = new GridBagLayout();
		gbl_panelHeader.columnWidths = new int[]{25, 150, 25, 0};
		gbl_panelHeader.rowHeights = new int[]{50, 0, 0, 0};
		gbl_panelHeader.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelHeader.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelHeader.setLayout(gbl_panelHeader);
		
		JLabel lblHeaderSubtitulo = new JLabel("Bienvenido a la clínica");
		lblHeaderSubtitulo.setForeground(Color.GRAY);
		lblHeaderSubtitulo.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
		GridBagConstraints gbc_lblHeaderSubtitulo = new GridBagConstraints();
		gbc_lblHeaderSubtitulo.insets = new Insets(0, 0, 5, 5);
		gbc_lblHeaderSubtitulo.gridx = 1;
		gbc_lblHeaderSubtitulo.gridy = 1;
		panelHeader.add(lblHeaderSubtitulo, gbc_lblHeaderSubtitulo);
		
		JLabel lblHeaderTitulo = new JLabel("San Antonio S.A");
		lblHeaderTitulo.setFont(new Font("DejaVu Sans", Font.BOLD, 16));
		GridBagConstraints gbc_lblHeaderTitulo = new GridBagConstraints();
		gbc_lblHeaderTitulo.insets = new Insets(0, 0, 0, 5);
		gbc_lblHeaderTitulo.gridx = 1;
		gbc_lblHeaderTitulo.gridy = 2;
		panelHeader.add(lblHeaderTitulo, gbc_lblHeaderTitulo);
		
		JPanel panelMain = new JPanel();
		panelMain.setBackground(new Color(51, 204, 255));
		contentPane.add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(new BoxLayout(panelMain, BoxLayout.Y_AXIS));
		
		panelMain.add(Box.createVerticalGlue());
		JButton btnRegistrarPersona = new JButton("Registrar una persona");
		btnRegistrarPersona.setAlignmentX(0.5f);
		panelMain.add(btnRegistrarPersona);
		btnRegistrarPersona.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		btnRegistrarPersona.setBackground(new Color(255, 255, 255));
		btnRegistrarPersona.setBorder(emptyBorder);
		
		panelMain.add(Box.createVerticalStrut(20));
		
		JButton btnConsultarPersonas = new JButton("Consultar personas");
		btnConsultarPersonas.setAlignmentX(0.5f);
		panelMain.add(btnConsultarPersonas);
		btnConsultarPersonas.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
		btnConsultarPersonas.setBackground(new Color(255, 255, 255));
		btnConsultarPersonas.setBorder(emptyBorder);
		panelMain.add(Box.createVerticalGlue());
		btnConsultarPersonas.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.onQueryClick();
			}
		});
		btnRegistrarPersona.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.onRegisterClick();
			}
		});
	}

}
