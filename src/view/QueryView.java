package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controllers.QueryViewController;
import enums.TipoOperario;
import events.EventListener;
import events.EventManager;
import vos.RegistryValue;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;

public class QueryView extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField inputDocumento;
	private JTable table;
	
	private QueryViewController controller;

	/**
	 * Create the dialog.
	 */
	public QueryView(javax.swing.JFrame parent, QueryViewController controller) {
		super(parent, "Consultar pacientes", java.awt.Dialog.ModalityType.DOCUMENT_MODAL);
		this.controller = controller;
		createComponents();
		
		controller.onQueryResults(new EventListener() {
			
			@Override
			public <T> void onEventFired(T arg) {
				RegistryValue[] values = (RegistryValue[]) arg;
				displayValues(values);
			}

		});
		
		addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent arg0) {
				controller.onQueryClick(null);
			}
			
			@Override
			public void componentResized(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}


	private void createComponents() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(51, 204, 255));
		contentPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JPanel queryDocumentPanel = new JPanel();
			queryDocumentPanel.setOpaque(false);
			contentPanel.add(queryDocumentPanel, BorderLayout.NORTH);
			queryDocumentPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5));
			{
				JLabel lblNumeroDeDocumento = new JLabel("Documento");
				queryDocumentPanel.add(lblNumeroDeDocumento);
			}
			{
				inputDocumento = new JTextField();
				queryDocumentPanel.add(inputDocumento);
				inputDocumento.setColumns(10);
			}
			{
				JButton btnConsultar = new JButton("Consultar");
				btnConsultar.setBackground(new Color(255, 255, 255));
				queryDocumentPanel.add(btnConsultar);
				btnConsultar.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						System.out.println("Query click");
						controller.onQueryClick(inputDocumento.getText());
					}
				});
			}
		}
		{
			JPanel queryResultsPanel = new JPanel();
			queryResultsPanel.setOpaque(false);
			contentPanel.add(queryResultsPanel, BorderLayout.CENTER);
			queryResultsPanel.setLayout(new BorderLayout(0, 0));
			{
				JLabel lblRegistros = new JLabel("Registros");
				lblRegistros.setFont(new Font("DejaVu Sans", Font.BOLD, 16));
				lblRegistros.setHorizontalAlignment(SwingConstants.CENTER);
				queryResultsPanel.add(lblRegistros, BorderLayout.NORTH);
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setAutoscrolls(true);
				queryResultsPanel.add(scrollPane, BorderLayout.CENTER);
				{
					table = new JTable(new DefaultTableModel(new Object[] {
							"NombreEmpresa",
							"Documento",
							"NombrePaciente",
							"DireccionPaciente",
							"Telefono",
							"TipoOperario",
							"NombreTratamiento",
							"CostoMedicamentos",
							"CostoTotal"
							}, 0));
					scrollPane.setViewportView(table);
				}
			}
			{
				JPanel buttonPane = new JPanel();
				queryResultsPanel.add(buttonPane, BorderLayout.SOUTH);
				buttonPane.setOpaque(false);
				buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
				{
					JButton cancelButton = new JButton("Salir");
					cancelButton.setBackground(Color.WHITE);
					cancelButton.setActionCommand("Cancel");
					buttonPane.add(cancelButton);
					JDialog dialog = this;
					cancelButton.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							controller.onExitClick(dialog);
						}
						
					});
				}
			}
		}
		{
			JPanel headerPanel = new JPanel();
			headerPanel.setBackground(Color.WHITE);
			getContentPane().add(headerPanel, BorderLayout.WEST);
			GridBagLayout gbl_headerPanel = new GridBagLayout();
			gbl_headerPanel.columnWidths = new int[]{25, 150, 25, 0};
			gbl_headerPanel.rowHeights = new int[]{50, 0, 0, 0};
			gbl_headerPanel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_headerPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			headerPanel.setLayout(gbl_headerPanel);
			{
				JLabel lblConsultar = new JLabel("Consultar");
				lblConsultar.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
				GridBagConstraints gbc_lblConsultar = new GridBagConstraints();
				gbc_lblConsultar.insets = new Insets(0, 0, 5, 5);
				gbc_lblConsultar.gridx = 1;
				gbc_lblConsultar.gridy = 1;
				headerPanel.add(lblConsultar, gbc_lblConsultar);
			}
			{
				JLabel lblPacientes = new JLabel("Pacientes");
				lblPacientes.setFont(new Font("DejaVu Sans", Font.BOLD, 16));
				lblPacientes.setForeground(Color.BLACK);
				GridBagConstraints gbc_lblPacientes = new GridBagConstraints();
				gbc_lblPacientes.insets = new Insets(0, 0, 0, 5);
				gbc_lblPacientes.gridx = 1;
				gbc_lblPacientes.gridy = 2;
				headerPanel.add(lblPacientes, gbc_lblPacientes);
			}
		}
	}
	

	private void displayValues(RegistryValue[] values) {
		if (table == null) return;
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
		for(RegistryValue value : values) {			
			model.addRow(new Object[] { 
					value.getNombreEmpresa(),
					value.getDocumentoPaciente(),
					value.getNombrePaciente(),
					value.getDireccionPaciente(),
					value.getTelefono(),
					TipoOperario.fromValue(value.getTipo()).name(),
					value.getNombreTratamiento(),
					value.getCostoMedicamentos(),
					value.getCostoTotal()
			});
		}
		
		repaint();
	}

}
