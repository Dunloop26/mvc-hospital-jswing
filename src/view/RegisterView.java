package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controllers.RegisterViewController;
import enums.TipoOperario;
import events.EventListener;
import vos.RegistryValue;
import vos.Treatment;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import java.awt.Font;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.border.LineBorder;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import java.awt.Dimension;

public class RegisterView extends JDialog {
	private RegisterViewController controller;
	private JTextField inputNombreEmpresa;
	private JTextField inputDocumentoPaciente;
	private JTextField inputNombrePaciente;
	private JTextField inputTelefono;
	private JTextField inputDireccion;
	private JTextField inputCostoMedicamentos;
	private JSpinner inputDiasHospitalizacion;
	private ComboBoxModel<Treatment> comboModel;
	private JComboBox<Treatment> inputTratamiento;

	private JRadioButton rdbtnOperario;
	private JRadioButton rdbtnMinero;

	private ButtonGroup radioBtnGroup;

	private Treatment[] treatments;
	private JLabel lblCostoResultado;
	private JButton btnVaciarCampos;

	private double currentUserFinalCost = 0;

	/**
	 * Create the dialog.
	 */
	public RegisterView(javax.swing.JFrame parent, RegisterViewController controller) {
		super(parent, "Registrar un paciente", java.awt.Dialog.ModalityType.DOCUMENT_MODAL);
		setMinimumSize(new Dimension(800, 600));
		this.controller = controller;

		controller.onTreatment(new EventListener() {

			@Override
			public <T> void onEventFired(T arg) {
				treatments = (Treatment[]) arg;
				System.out.println(treatments.length);
				inputTratamiento.removeAllItems();
				for (Treatment treatment : treatments) {
					inputTratamiento.addItem(treatment);
				}

				System.out.println("Treatments updated");
			}
		});

		controller.onCostResult(new EventListener() {

			@Override
			public <T> void onEventFired(T arg) {
				double cost = (double) arg;
				currentUserFinalCost = cost;
				setCostResultLabel(cost);
			}

		});

		createComponents(controller);

		addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent arg0) {
				// Actualizo la información de los tratamientos del controlador
				controller.requestTreatments();
			}

			@Override
			public void componentResized(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	private void createComponents(RegisterViewController controller) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 550);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel headerPane = new JPanel();
			headerPane.setBorder(new EmptyBorder(0, 0, 0, 0));
			headerPane.setBackground(Color.WHITE);
			getContentPane().add(headerPane, BorderLayout.WEST);
			{
				GridBagLayout gbl_headerPane = new GridBagLayout();
				gbl_headerPane.columnWidths = new int[] { 25, 150, 25, 0 };
				gbl_headerPane.rowHeights = new int[] { 50, 0, 0, 0 };
				gbl_headerPane.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
				gbl_headerPane.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
				headerPane.setLayout(gbl_headerPane);
				{
					JLabel lblRegistroDePacientes_1 = new JLabel("Registro de");
					lblRegistroDePacientes_1.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
					GridBagConstraints gbc_lblRegistroDePacientes_1 = new GridBagConstraints();
					gbc_lblRegistroDePacientes_1.insets = new Insets(0, 0, 5, 5);
					gbc_lblRegistroDePacientes_1.gridx = 1;
					gbc_lblRegistroDePacientes_1.gridy = 1;
					headerPane.add(lblRegistroDePacientes_1, gbc_lblRegistroDePacientes_1);
				}
				{
					JLabel lblRegistroDePacientes_1 = new JLabel("Pacientes");
					lblRegistroDePacientes_1.setFont(new Font("DejaVu Sans", Font.BOLD, 16));
					GridBagConstraints gbc_lblRegistroDePacientes_1 = new GridBagConstraints();
					gbc_lblRegistroDePacientes_1.insets = new Insets(0, 0, 0, 5);
					gbc_lblRegistroDePacientes_1.gridx = 1;
					gbc_lblRegistroDePacientes_1.gridy = 2;
					headerPane.add(lblRegistroDePacientes_1, gbc_lblRegistroDePacientes_1);
				}
			}
		}
		{
			JPanel mainPanel = new JPanel();
			mainPanel.setBackground(new Color(51, 204, 255));
			mainPanel.setBorder(null);
			getContentPane().add(mainPanel, BorderLayout.CENTER);
			mainPanel.setLayout(new BorderLayout(0, 0));
			{
				JPanel contentPanel = new JPanel();
				contentPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
				contentPanel.setBackground(new Color(51, 204, 255));
				mainPanel.add(contentPanel, BorderLayout.CENTER);
				GridBagLayout gbl_contentPanel = new GridBagLayout();
				gbl_contentPanel.columnWidths = new int[] { 37, 0, 0, 0 };
				gbl_contentPanel.rowHeights = new int[] { 15, 0, 35, 25, 10, 0, 0, 10, 0, 0, 10, 0, 10, 0, 0, 0, 5, 25,
						5, 20, 0 };
				gbl_contentPanel.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
				gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
						0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE };
				contentPanel.setLayout(gbl_contentPanel);
				{
					JLabel lblNewLabel_1 = new JLabel("Formulario");
					lblNewLabel_1.setFont(new Font("DejaVu Sans", Font.BOLD, 20));
					lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
					GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
					gbc_lblNewLabel_1.gridwidth = 3;
					gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
					gbc_lblNewLabel_1.gridx = 0;
					gbc_lblNewLabel_1.gridy = 1;
					contentPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);
				}
				{
					JLabel lblNombreempresa = new JLabel("Nombre de la empresa*");
					lblNombreempresa.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					GridBagConstraints gbc_lblNombreempresa = new GridBagConstraints();
					gbc_lblNombreempresa.fill = GridBagConstraints.VERTICAL;
					gbc_lblNombreempresa.anchor = GridBagConstraints.WEST;
					gbc_lblNombreempresa.insets = new Insets(0, 0, 5, 5);
					gbc_lblNombreempresa.gridx = 0;
					gbc_lblNombreempresa.gridy = 3;
					contentPanel.add(lblNombreempresa, gbc_lblNombreempresa);
				}
				{
					inputNombreEmpresa = new JTextField();
					inputNombreEmpresa.setColumns(10);
					GridBagConstraints gbc_inputNombreEmpresa = new GridBagConstraints();
					gbc_inputNombreEmpresa.fill = GridBagConstraints.HORIZONTAL;
					gbc_inputNombreEmpresa.insets = new Insets(0, 0, 5, 0);
					gbc_inputNombreEmpresa.gridx = 2;
					gbc_inputNombreEmpresa.gridy = 3;
					contentPanel.add(inputNombreEmpresa, gbc_inputNombreEmpresa);
				}
				{
					JSeparator separator = new JSeparator();
					GridBagConstraints gbc_separator = new GridBagConstraints();
					gbc_separator.gridwidth = 3;
					gbc_separator.insets = new Insets(0, 0, 5, 0);
					gbc_separator.gridx = 0;
					gbc_separator.gridy = 4;
					contentPanel.add(separator, gbc_separator);
				}
				{
					JLabel lblNombre = new JLabel("Documento paciente*");
					lblNombre.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					GridBagConstraints gbc_lblNombre = new GridBagConstraints();
					gbc_lblNombre.anchor = GridBagConstraints.WEST;
					gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
					gbc_lblNombre.gridx = 0;
					gbc_lblNombre.gridy = 5;
					contentPanel.add(lblNombre, gbc_lblNombre);
				}
				{
					inputDocumentoPaciente = new JTextField();
					inputDocumentoPaciente.setColumns(10);
					GridBagConstraints gbc_inputDocumentoPaciente = new GridBagConstraints();
					gbc_inputDocumentoPaciente.fill = GridBagConstraints.HORIZONTAL;
					gbc_inputDocumentoPaciente.insets = new Insets(0, 0, 5, 0);
					gbc_inputDocumentoPaciente.gridx = 2;
					gbc_inputDocumentoPaciente.gridy = 5;
					contentPanel.add(inputDocumentoPaciente, gbc_inputDocumentoPaciente);
				}
				{
					JLabel lblNombrePaciente = new JLabel("Nombre del paciente*");
					lblNombrePaciente.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					GridBagConstraints gbc_lblNombrePaciente = new GridBagConstraints();
					gbc_lblNombrePaciente.anchor = GridBagConstraints.WEST;
					gbc_lblNombrePaciente.insets = new Insets(0, 0, 5, 5);
					gbc_lblNombrePaciente.gridx = 0;
					gbc_lblNombrePaciente.gridy = 6;
					contentPanel.add(lblNombrePaciente, gbc_lblNombrePaciente);
				}
				{
					inputNombrePaciente = new JTextField();
					inputNombrePaciente.setHorizontalAlignment(SwingConstants.LEFT);
					inputNombrePaciente.setColumns(10);
					inputNombrePaciente.setAlignmentX(0.0f);
					GridBagConstraints gbc_inputNombrePaciente = new GridBagConstraints();
					gbc_inputNombrePaciente.fill = GridBagConstraints.HORIZONTAL;
					gbc_inputNombrePaciente.insets = new Insets(0, 0, 5, 0);
					gbc_inputNombrePaciente.gridx = 2;
					gbc_inputNombrePaciente.gridy = 6;
					contentPanel.add(inputNombrePaciente, gbc_inputNombrePaciente);
				}
				{
					JLabel lblDireccion = new JLabel("Dirección*");
					lblDireccion.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					GridBagConstraints gbc_lblDireccion = new GridBagConstraints();
					gbc_lblDireccion.anchor = GridBagConstraints.WEST;
					gbc_lblDireccion.insets = new Insets(0, 0, 5, 5);
					gbc_lblDireccion.gridx = 0;
					gbc_lblDireccion.gridy = 8;
					contentPanel.add(lblDireccion, gbc_lblDireccion);
				}
				{
					inputDireccion = new JTextField();
					GridBagConstraints gbc_inputDireccion = new GridBagConstraints();
					gbc_inputDireccion.insets = new Insets(0, 0, 5, 0);
					gbc_inputDireccion.fill = GridBagConstraints.HORIZONTAL;
					gbc_inputDireccion.gridx = 2;
					gbc_inputDireccion.gridy = 8;
					contentPanel.add(inputDireccion, gbc_inputDireccion);
					inputDireccion.setColumns(10);
				}
				{
					JLabel lblTelefono = new JLabel("Telefono*");
					lblTelefono.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					GridBagConstraints gbc_lblTelefono = new GridBagConstraints();
					gbc_lblTelefono.anchor = GridBagConstraints.WEST;
					gbc_lblTelefono.insets = new Insets(0, 0, 5, 5);
					gbc_lblTelefono.gridx = 0;
					gbc_lblTelefono.gridy = 9;
					contentPanel.add(lblTelefono, gbc_lblTelefono);
				}
				{
					inputTelefono = new JTextField();
					GridBagConstraints gbc_inputTelefono = new GridBagConstraints();
					gbc_inputTelefono.insets = new Insets(0, 0, 5, 0);
					gbc_inputTelefono.fill = GridBagConstraints.HORIZONTAL;
					gbc_inputTelefono.gridx = 2;
					gbc_inputTelefono.gridy = 9;
					contentPanel.add(inputTelefono, gbc_inputTelefono);
					inputTelefono.setColumns(10);
				}
				{
					JPanel panel = new JPanel();
					panel.setOpaque(false);
					panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 153, 255), 1, true), "Tipo operario",
							TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
					panel.setBackground(new Color(51, 204, 255));
					GridBagConstraints gbc_panel = new GridBagConstraints();
					gbc_panel.gridwidth = 3;
					gbc_panel.insets = new Insets(0, 0, 5, 0);
					gbc_panel.fill = GridBagConstraints.BOTH;
					gbc_panel.gridx = 0;
					gbc_panel.gridy = 11;
					contentPanel.add(panel, gbc_panel);
					panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
					{
						rdbtnOperario = new JRadioButton("Operario", true);
						rdbtnOperario.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
						rdbtnOperario.setBackground(new Color(51, 204, 255));
						panel.add(rdbtnOperario);
						rdbtnOperario.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent arg0) {
								validateTotalCost();
							}
						});
					}
					{
						Component horizontalStrut = Box.createHorizontalStrut(20);
						panel.add(horizontalStrut);
					}
					{
						rdbtnMinero = new JRadioButton("Minero", false);
						rdbtnMinero.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
						rdbtnMinero.setBackground(new Color(51, 204, 255));
						panel.add(rdbtnMinero);
						rdbtnMinero.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent arg0) {
								validateTotalCost();
							}
						});
					}

					radioBtnGroup = new ButtonGroup();
					radioBtnGroup.add(rdbtnOperario);
					radioBtnGroup.add(rdbtnMinero);
				}
				{
					JLabel lblDiasHopitalizacin = new JLabel("Dias hopitalización");
					lblDiasHopitalizacin.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					GridBagConstraints gbc_lblDiasHopitalizacin = new GridBagConstraints();
					gbc_lblDiasHopitalizacin.anchor = GridBagConstraints.WEST;
					gbc_lblDiasHopitalizacin.insets = new Insets(0, 0, 5, 5);
					gbc_lblDiasHopitalizacin.gridx = 0;
					gbc_lblDiasHopitalizacin.gridy = 13;
					contentPanel.add(lblDiasHopitalizacin, gbc_lblDiasHopitalizacin);
				}
				{
					inputDiasHospitalizacion = new JSpinner();
					GridBagConstraints gbc_inputDiasHospitalizacion = new GridBagConstraints();
					gbc_inputDiasHospitalizacion.anchor = GridBagConstraints.WEST;
					gbc_inputDiasHospitalizacion.insets = new Insets(0, 0, 5, 0);
					gbc_inputDiasHospitalizacion.gridx = 2;
					gbc_inputDiasHospitalizacion.gridy = 13;
					contentPanel.add(inputDiasHospitalizacion, gbc_inputDiasHospitalizacion);
					inputDiasHospitalizacion.addChangeListener(new ChangeListener() {

						@Override
						public void stateChanged(ChangeEvent arg0) {
							if ((int) inputDiasHospitalizacion.getValue() < 0) {
								inputDiasHospitalizacion.setValue(0);
							}

							validateTotalCost();
						}
					});
				}
				{
					JLabel lblTratamiento = new JLabel("Tratamiento");
					lblTratamiento.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					lblTratamiento.setHorizontalAlignment(SwingConstants.LEFT);
					GridBagConstraints gbc_lblTratamiento = new GridBagConstraints();
					gbc_lblTratamiento.anchor = GridBagConstraints.WEST;
					gbc_lblTratamiento.insets = new Insets(0, 0, 5, 5);
					gbc_lblTratamiento.gridx = 0;
					gbc_lblTratamiento.gridy = 14;
					contentPanel.add(lblTratamiento, gbc_lblTratamiento);
				}
				{
					comboModel = new DefaultComboBoxModel<Treatment>();
					inputTratamiento = new JComboBox<Treatment>(comboModel);
					GridBagConstraints gbc_inputTratamiento = new GridBagConstraints();
					gbc_inputTratamiento.insets = new Insets(0, 0, 5, 0);
					gbc_inputTratamiento.fill = GridBagConstraints.HORIZONTAL;
					gbc_inputTratamiento.gridx = 2;
					gbc_inputTratamiento.gridy = 14;
					contentPanel.add(inputTratamiento, gbc_inputTratamiento);

					inputTratamiento.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent arg0) {
							if (inputTratamiento.getItemCount() == 0) return;
							validateTotalCost();
						}
					});
				}
				{
					JLabel lblCostoMedicamentos = new JLabel("Costo medicamentos");
					lblCostoMedicamentos.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
					GridBagConstraints gbc_lblCostoMedicamentos = new GridBagConstraints();
					gbc_lblCostoMedicamentos.anchor = GridBagConstraints.WEST;
					gbc_lblCostoMedicamentos.insets = new Insets(0, 0, 5, 5);
					gbc_lblCostoMedicamentos.gridx = 0;
					gbc_lblCostoMedicamentos.gridy = 15;
					contentPanel.add(lblCostoMedicamentos, gbc_lblCostoMedicamentos);
				}
				{

					inputCostoMedicamentos = new JTextField();
					inputCostoMedicamentos.setText("0");
					inputCostoMedicamentos.getDocument().addDocumentListener(new DocumentListener() {

						@Override
						public void removeUpdate(DocumentEvent ev) {
							validateTotalCost();
						}

						@Override
						public void insertUpdate(DocumentEvent ev) {
							validateTotalCost();
						}

						@Override
						public void changedUpdate(DocumentEvent ev) {

						}
					});
					GridBagConstraints gbc_inputCostoMedicamentos = new GridBagConstraints();
					gbc_inputCostoMedicamentos.insets = new Insets(0, 0, 5, 0);
					gbc_inputCostoMedicamentos.fill = GridBagConstraints.HORIZONTAL;
					gbc_inputCostoMedicamentos.gridx = 2;
					gbc_inputCostoMedicamentos.gridy = 15;
					contentPanel.add(inputCostoMedicamentos, gbc_inputCostoMedicamentos);
					inputCostoMedicamentos.setColumns(10);
				}
				{
					JPanel panel = new JPanel();
					panel.setOpaque(false);
					panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 153, 255)), "Calculo costo",
							TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
					GridBagConstraints gbc_panel = new GridBagConstraints();
					gbc_panel.gridwidth = 3;
					gbc_panel.insets = new Insets(0, 0, 5, 0);
					gbc_panel.fill = GridBagConstraints.BOTH;
					gbc_panel.gridx = 0;
					gbc_panel.gridy = 17;
					contentPanel.add(panel, gbc_panel);
					panel.setLayout(new BorderLayout(0, 0));
					{
						lblCostoResultado = new JLabel("Ingrese los valores de costo");
						lblCostoResultado.setFont(new Font("DejaVu Sans", Font.BOLD, 16));
						lblCostoResultado.setHorizontalAlignment(SwingConstants.CENTER);
						panel.add(lblCostoResultado);
					}
				}
				{
					JPanel buttonPanel = new JPanel();
					FlowLayout flowLayout = (FlowLayout) buttonPanel.getLayout();
					flowLayout.setAlignment(FlowLayout.RIGHT);
					buttonPanel.setOpaque(false);
					GridBagConstraints gbc_buttonPanel = new GridBagConstraints();
					gbc_buttonPanel.anchor = GridBagConstraints.EAST;
					gbc_buttonPanel.gridwidth = 3;
					gbc_buttonPanel.gridx = 0;
					gbc_buttonPanel.gridy = 19;
					contentPanel.add(buttonPanel, gbc_buttonPanel);
					{
						btnVaciarCampos = new JButton("Vaciar Campos");
						btnVaciarCampos.setBackground(new Color(255, 255, 255));
						buttonPanel.add(btnVaciarCampos);
						btnVaciarCampos.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent arg0) {
								cleanForm();
							}
						});
					}
					{
						JButton btnRegistrarUsuario = new JButton("Registrar usuario");
						btnRegistrarUsuario.setBackground(Color.WHITE);
						btnRegistrarUsuario.setAlignmentX(0.5f);
						btnRegistrarUsuario.setActionCommand("Cancel");
						buttonPanel.add(btnRegistrarUsuario);
						btnRegistrarUsuario.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent arg0) {
								RegistryValue registryValue = buildRegistryValue();
								controller.onRegisterEntry(registryValue);
							}

						});
					}
					{
						JButton cancelButton = new JButton("Salir");
						cancelButton.setBackground(Color.WHITE);
						cancelButton.setAlignmentX(0.5f);
						cancelButton.setActionCommand("Cancel");
						buttonPanel.add(cancelButton);
						JDialog dialog = this;
						cancelButton.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent arg0) {
								controller.onExitClick(dialog);
							}
						});
					}
				}
			}
		}
	}

	private void setCostResultLabel(double cost) {
		Locale locale = new Locale("es_CO");
		Currency peso = Currency.getInstance("COP");
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
		numberFormat.setCurrency(peso);
		lblCostoResultado.setForeground(Color.BLACK);
		lblCostoResultado.setText(numberFormat.format(cost));
	}

	public void cleanForm() {
		inputCostoMedicamentos.setText("0");
		inputDiasHospitalizacion.setValue(0);
		inputTratamiento.setSelectedIndex(0);
		inputNombreEmpresa.setText("");
		inputNombrePaciente.setText("");
		inputDocumentoPaciente.setText("");
		inputDireccion.setText("");
		inputTelefono.setText("");
	}

	private void validateTotalCost() {		

		try {
			// Obtengo las variables de cálculo
			int dias = (int) inputDiasHospitalizacion.getValue();
			double costoMedicamentos = Double.parseDouble(inputCostoMedicamentos.getText());
			Treatment tratamientoSeleccionado = (Treatment) inputTratamiento.getSelectedItem();

			TipoOperario tipo = getSelectedOperatorType();

			// Envio los datos de cálculo de costos
			controller.calculateCosts(dias, costoMedicamentos, tratamientoSeleccionado, tipo);
		} catch (NumberFormatException e) {
			System.out.print("Number convertion error: ");
			e.printStackTrace();
			lblCostoResultado.setForeground(new Color(220, 20, 20));
			lblCostoResultado.setText("No se puede calcular.");
		}
	}

	private TipoOperario getSelectedOperatorType() {

		// Obtengo los dos botones radiales
		if (rdbtnMinero.isSelected())
			return TipoOperario.MINERO;
		else if (rdbtnOperario.isSelected())
			return TipoOperario.OPERARIO;
		else
			return TipoOperario.NINGUNO;

	}

	private boolean validTreatmentIndex(int index) {
		return (treatments != null && index >= 0 && index < treatments.length);
	}

	private RegistryValue buildRegistryValue() {
		// Obtengo los valores desde los input
		try {
			String nombreEmpresa = inputNombreEmpresa.getText();
			String documentoPaciente = inputDocumentoPaciente.getText();
			String nombrePaciente = inputNombrePaciente.getText();
			String direccion = inputDireccion.getText();
			String telefono = inputTelefono.getText();
			int diasHospitalizacion = (int) inputDiasHospitalizacion.getValue();
			double costoMedicamentos = Double.parseDouble(inputCostoMedicamentos.getText());
			int indiceTratamiento = inputTratamiento.getSelectedIndex();
			String nombreTratamiento = "";
			TipoOperario tipoOperario = getSelectedOperatorType();

			if (nombreEmpresa.isBlank() || documentoPaciente.isBlank() || nombrePaciente.isBlank()
					|| direccion.isBlank() || telefono.isBlank()) {
				JOptionPane.showMessageDialog(this,
						"No se ha podido registrar el usuario, algunos campos necesarios están vacíos");
			}

			if (validTreatmentIndex(indiceTratamiento)) {
				nombreTratamiento = treatments[indiceTratamiento].getName();
			}

			return new RegistryValue(nombreEmpresa, documentoPaciente, nombrePaciente, direccion, telefono,
					tipoOperario.getValue(), nombreTratamiento, costoMedicamentos, currentUserFinalCost);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "No se ha podido registrar el usuario");
			return null;
		}
	}
}
