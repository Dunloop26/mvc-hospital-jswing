package vos;

public class RegistryValue {

	private final String nombreEmpresa;
	private final String documentoPaciente;
	private final String nombrePaciente;
	private final String direccionPaciente;
	private final String telefono;
	private final int tipo;
	private final String nombreTratamiento;
	private final double costoMedicamentos;
	private final double costoTotal;

	public RegistryValue(String nombreEmpresa, String documentoPaciente, String nombrePaciente,
			String direccionPaciente, String telefono, int tipo, String nombreTratamiento, double costoMedicamentos2,
			double costoFinal) {
		this.nombreEmpresa = nombreEmpresa;
		this.documentoPaciente = documentoPaciente;
		this.nombrePaciente = nombrePaciente;
		this.direccionPaciente = direccionPaciente;
		this.telefono = telefono;
		this.tipo = tipo;
		this.nombreTratamiento = nombreTratamiento;
		this.costoMedicamentos = costoMedicamentos2;
		this.costoTotal = costoFinal;
	}


	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public String getDocumentoPaciente() {
		return documentoPaciente;
	}

	public String getNombrePaciente() {
		return nombrePaciente;
	}

	public String getDireccionPaciente() {
		return direccionPaciente;
	}

	public String getTelefono() {
		return telefono;
	}

	public int getTipo() {
		return tipo;
	}

	public String getNombreTratamiento() {
		return nombreTratamiento;
	}

	public double getCostoMedicamentos() {
		return costoMedicamentos;
	}

	public double getCostoTotal() {
		return costoTotal;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegistryValue [nombreEmpresa=");
		builder.append(nombreEmpresa);
		builder.append(", documentoPaciente=");
		builder.append(documentoPaciente);
		builder.append(", nombrePaciente=");
		builder.append(nombrePaciente);
		builder.append(", direccionPaciente=");
		builder.append(direccionPaciente);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", tipo=");
		builder.append(tipo);
		builder.append(", nombreTratamiento=");
		builder.append(nombreTratamiento);
		builder.append(", costoMedicamentos=");
		builder.append(costoMedicamentos);
		builder.append(", costoTotal=");
		builder.append(costoTotal);
		builder.append("]");
		return builder.toString();
	}
	

}
