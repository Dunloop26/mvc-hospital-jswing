package vos;

public class CostCalculation {

	private final int days;
	private final double medicineCost;
	private final double treatmentCost;
	private final int tipoOperario;

	public CostCalculation(int days, double medicineCost, double treatmentCost, int tipoOperario) {
		this.days = days;
		this.medicineCost = medicineCost;
		this.treatmentCost = treatmentCost;
		this.tipoOperario = tipoOperario;
	}

	public int getDays() {
		return days;
	}

	public double getMedicineCost() {
		return medicineCost;
	}

	public double getTreatmentCost() {
		return treatmentCost;
	}
	
	public int getTipoOperario() {
		return tipoOperario;
	}

}
