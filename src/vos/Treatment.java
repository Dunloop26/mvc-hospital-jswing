package vos;

import java.text.NumberFormat;
import java.util.Currency;

public class Treatment {
	public Treatment(String id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	private final String id;
	private final String name;
	private final double price;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append(": ");
		NumberFormat fmt = NumberFormat.getCurrencyInstance();
		fmt.setCurrency(Currency.getInstance("COP"));
		String priceValue = fmt.format(price);
		sb.append(priceValue);
		return sb.toString();
	}

}
