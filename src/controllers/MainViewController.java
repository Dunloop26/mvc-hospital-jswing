package controllers;

import events.IEventManager;
import view.MainView;

public class MainViewController {

	private IEventManager eventManager;

	public MainViewController(IEventManager eventManager) {
		this.eventManager = eventManager;
	}

	public void onRegisterClick() {
		eventManager.fireEvent("openRegisterView", null);
	}
	
	public void onQueryClick() {
		eventManager.fireEvent("openQueryView", null);
	}
}
