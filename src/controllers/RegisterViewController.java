package controllers;

import javax.swing.JDialog;

import enums.TipoOperario;
import events.EventListener;
import events.IEventManager;
import vos.CostCalculation;
import vos.RegistryValue;
import vos.Treatment;

public class RegisterViewController {

	private IEventManager eventManager;

	public RegisterViewController(IEventManager eventManager) {
		this.eventManager = eventManager;
	}

	public void onExitClick(JDialog frame) {
		eventManager.fireEvent("closeRegisterView", null);
		frame.dispose();
	}

	public void onRegisterEntry(RegistryValue value) {
		if (value == null)
			return;
		eventManager.fireEvent("registerUser", value);
	}

	public void requestTreatments() {
		eventManager.fireEvent("requestTreatment", null);
	}

	public void onTreatment(EventListener listener) {
		eventManager.addEvent("onTreatment", listener);
	}
	
	public void onCostResult(EventListener listener) {
		eventManager.addEvent("onCostResult", listener);
	}

	public void calculateCosts(int dias, double costoMedicamentos, Treatment tratamientoSeleccionado, TipoOperario tipoOperario) {
		CostCalculation cost = new CostCalculation(dias, costoMedicamentos, tratamientoSeleccionado.getPrice(), tipoOperario.getValue());
		eventManager.fireEvent("requestCostCalculation", cost);
	}
}
