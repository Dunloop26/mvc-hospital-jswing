package controllers;

import javax.swing.JDialog;

import events.EventListener;
import events.IEventManager;

public class QueryViewController {
	
	private IEventManager eventManager;
	
	public QueryViewController(IEventManager eventManager) {
		this.eventManager = eventManager;
	}
	
	public void onQueryClick(String queryString) {
		eventManager.fireEvent("requestQuery", queryString);
	}
	
	public void onQueryResults(EventListener eventListener) {
		if (eventListener == null) return;
		eventManager.addEvent("onQueryResults", eventListener);
	}

	public void onExitClick(JDialog dialog) {
		eventManager.fireEvent("closeQueryView", null);
		dialog.dispose();
	}
}
