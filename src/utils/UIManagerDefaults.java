package utils;

import java.util.Enumeration;

import javax.swing.UIManager;

public class UIManagerDefaults {
	public static void setUIFont(javax.swing.plaf.FontUIResource fontResource) {
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if(value instanceof javax.swing.plaf.FontUIResource) {
				UIManager.put(key, fontResource);
			}
		}
	}
}
