package enums;

public enum TipoOperario {
	NINGUNO(0), OPERARIO(1), MINERO(2);

	private final int value;
	private TipoOperario(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}

	public static TipoOperario fromValue(int value) {
		switch(value) {
			case 1:
				return TipoOperario.OPERARIO;
			case 2: 
				return TipoOperario.MINERO;
			default:
				return TipoOperario.NINGUNO;
		}
	}
}
