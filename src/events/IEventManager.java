package events;

public interface IEventManager {
	boolean hasEvent(String eventId);
	void addEvent(String id, EventListener listener);
	void clearEvent(String id);
	void clearEvents();
	<T> void fireEvent(String id, T eventArg);
}
