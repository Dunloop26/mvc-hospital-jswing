package events;

public interface EventListener {
	<T> void onEventFired(T arg);
}
