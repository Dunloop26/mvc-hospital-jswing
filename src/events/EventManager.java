package events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class EventManager implements IEventManager {
	
	private HashMap<String, ArrayList<EventListener>> eventMap = new HashMap<>();
	
	public boolean hasEvent(String eventId) {
		return eventMap.containsKey(eventId);
	}
	
	public void addEvent(String id, EventListener listener) {
		// NO añado el evento si id está vacio o en blanco, o el listener es nulo
		if (!validId(id) || listener == null) return;
		// Si el evento ya está registrado
		if (eventMap.containsKey(id)) {
			ArrayList<EventListener> eventList = eventMap.get(id);
			// Agrego el nuevo evento
			eventList.add(listener);
		} else {
			// Agrego una nueva lista de eventos con el id
			eventMap.put(id, new ArrayList<EventListener>(Arrays.asList(listener)));
		}
	}
	
	public void clearEvent(String id) {
		// Si el mapa de eventos NO contiene el ID, cancelo
		if (!eventMap.containsKey(id)) return;
		// Remuevo el evento del mapa de eventos
		eventMap.remove(id);
	}
	
	public void clearEvents() {
		// Limpio los eventos
		eventMap.clear();
	}
	
	public <T> void fireEvent(String id, T arg) {
		// Si NO está registrado un evento
		if (!eventMap.containsKey(id)) return;
		// Obtengo la lista de eventos
		ArrayList<EventListener> eventList = eventMap.get(id);
		// Ejecuto cada evento de la lista
		for(EventListener listener : eventList) {
			listener.onEventFired(arg);
		}
	}
	
	private boolean validId(String id) {
		// Verifico si el id es válido
		return id != null && !id.isEmpty() && !id.isBlank();
	}
	
}
